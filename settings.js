const fs = require("fs");
const p = require("path");

module.exports = class {
  constructor(path){
    if(!fs.existsSync(path))
      fs.writeFileSync(path, JSON.stringify({
        "database_location": p.resolve("./database.json"),
        "songs_folder": p.resolve("./songs/"),
        "writeMetaDefault": true
      }));
    this.path = path;
    this.settings = JSON.parse(fs.readFileSync(path));
    };

  save(){
    return new Promise((resolve, reject) => {
      fs.writeFile(this.path, JSON.stringify(this.settings), err => {
        if(err) reject(err);
        else resolve();
      })
    });
  }

  reset(){
    return new Promise((resolve, reject) => {
      this.settings = {
        "database_location": p.resolve("./database.json"),
        "songs_folder": p.resolve("./songs/"),
        "writeMetaDefault": true
      };
      fs.writeFile(path, JSON.stringify(this.settings), err => {
        if(err) reject(err);
        else resolve();
      });
    })
  }
}
