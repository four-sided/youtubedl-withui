const {app, BrowserWindow, ipcMain} = require("electron");
const path = require("path");
const youtubedownloader = require("ytdownloader");
let settings = require("./settings.js");

settings = new settings(path.resolve("./settings.json"));
let win;
let ytdl = new youtubedownloader(!app.isPackaged, settings.settings.songs_folder, settings.settings.database_location);

function createWindow(){
  win = new BrowserWindow({show: false, width: 800, height: 600, autoHideMenuBar: true, webPreferences: {nodeIntegration: true} });

  win.loadFile("views/index.html");

  win.on("ready-to-show", () => {
    win.show();
  });

  win.on("closed", () => {
    win = null;
  });
}

ipcMain.on('add_song_url', (event, url, writeMeta, audioOnly) => {
  try {
  ytdl.download(url, audioOnly, writeMeta, (error, info) => {
    if(!error){
      event.sender.send("song_added", false, info);
    }else {
      event.sender.send("song_added", error);
    }});
  } catch (err){
    event.sender.send("song_added", err);
  }

});

ipcMain.on('get_settings', (event, arg) => {
  event.returnValue = settings.settings;
});

ipcMain.on('set_settings', (event, setings) => {
  settings.settings = setings;
  ytdl = new youtubedownloader(true, settings.songs_folder, settings.database_location);
  settings.save()
  .then(() => {event.sender.send("settings_set", true)})
  .catch(err => {event.sender.send("settings_set", err)});
});

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if(process.platform !== 'darwin')
    app.quit();
});

app.on("activate", () => {
  if(win == null)
    createWindow();
});
