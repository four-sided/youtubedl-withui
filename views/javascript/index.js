window.onload = () => {
  const { ipcRenderer } = require('electron')
  const add_song_url = document.getElementById("url");
  const add_song_url_btn = document.getElementById("url_btn");
  const settings = ipcRenderer.sendSync("get_settings");

  add_song_url_btn.addEventListener("click", function(){

    ipcRenderer.once('song_added', (event, error, song) => {
      if(error) {
        console.log(error);
        toaster("Error while downloading Song! See console for more information.", "error");
      } else {
        toaster("Song " + song.title + " added!", "success");
      }
      add_song_url_btn.disabled = false;
      add_song_url_btn.classList.remove("animated");
      add_song_url_btn.classList.remove("loading");
      add_song_url_btn.value = "";
    });
    ipcRenderer.send('add_song_url', add_song_url.value, document.getElementById("writeMeta").checked, document.getElementById("audioOnly").checked);
    add_song_url_btn.disabled = true;
    add_song_url_btn.classList.add("animated");
    add_song_url_btn.classList.add("loading");
  });


  window.onhashchange = event => {
    if(window.location.hash == "#mAddSong")
      add_song_url.focus();
      if(settings.writeMetaDefault){
        document.getElementById("writeMeta").checked = true;
        document.getElementById("audioOnly").checked = true;
      }
  }


};
