const {ipcRenderer} = require("electron");

window.onload = () => {
  let songs_folder = document.getElementById("songs_folder");
  let database_location = document.getElementById("database_location");
  let default_meta = document.getElementById("default_meta");
  let submit_changes = document.getElementById("submit_changes");

  let settings = ipcRenderer.sendSync("get_settings");
  songs_folder.value = settings.songs_folder;
  database_location.value = settings.database_location;
  default_meta.checked = settings.writeMetaDefault;

  let check_changes = () => {
    submit_changes.disabled = (settings.songs_folder == songs_folder.value && settings.database_location == database_location.value && default_meta.checked == settings.writeMetaDefault)
  }

  songs_folder.onchange = check_changes;
  database_location.onchange = check_changes;
  default_meta.onchange = check_changes;

  submit_changes.onclick = () => {
    settings.songs_folder = songs_folder.value;
    settings.database_location = database_location.value;
    settings.writeMetaDefault = default_meta.checked;
    ipcRenderer.once("settings_set", (event, result) => {
      if(result === true){
        console.log("Settings set!");
      } else {
        console.log("An error occured while trying to set the settings!");
      }
    });
    ipcRenderer.send("set_settings", settings);
  }
}
