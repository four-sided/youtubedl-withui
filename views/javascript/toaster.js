var last_toast_id = 0;


function toaster(message, type) {
  let toastholder = document.getElementById("toastholder");
  let id = last_toast_id++;
  let toast = `<button class="btn-close" onclick="jumptoastout('toast-${id}')"></button><p>${message}</p>`;

  let elem = document.createElement("div");
  elem.className +="toast animated bounceIn "+type;
  elem.id = "toast-"+id;
  elem.innerHTML = toast;


  toastholder.appendChild(elem);

  console.log("Toast: "+toast, "\bToastElem: "+elem);
}



function jumptoastout(id) {
  let toastholder = document.getElementById("toastholder");
  toastholder.removeChild(document.getElementById(id));
}
/*
module.exports.jumptoastout = jumptoastout;
module.exports.toaster = toaster;
*/
